using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindManager : MonoBehaviour
{
    List<GameObject> Doors = new List<GameObject>();
    public GameObject Walls;

    public float windInterval = 5f;
    public float windStrength = 25f;
    public int maxOosh = 7;
    private float windTimer;

    // Start is called before the first frame update
    void Start()
    {
        windTimer = windInterval;


       foreach(Transform t in Walls.GetComponentInChildren<Transform>())
        {
            if (t.gameObject.name.ToLower().Contains("door"))
            {
                Doors.Add(t.gameObject);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        windTimer -= Time.deltaTime;
        if(windTimer < 0)
        {
            Vector2 windDirection = windStrength * RandomOnUnitCircle();
            if(Doors.Count > 0)
            {
                string woosh = "WO" + new string('O', Random.Range(1, maxOosh)) + "SH";
                NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.small, woosh ,Doors[Random.Range(0, Doors.Count - 1)].transform.position);
                foreach(GameObject gameObject in Doors)
                {
                    foreach (Rigidbody2D rigidbody2D in gameObject.GetComponentsInChildren<Rigidbody2D>())
                        rigidbody2D.AddForce(windDirection);
                }
            }
            windTimer = windInterval;
        }
    }

    Vector2 RandomOnUnitCircle()
    {
        float angle = Random.Range(0, 2 * Mathf.PI);
        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
    }
}


