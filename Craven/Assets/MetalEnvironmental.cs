﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalEnvironmental : Environmental
{
    [HideInInspector] Vector3 lastposition;
    [HideInInspector] bool moving;
    public float delay = .23f;
    public float mindistance = .1f;
    public float textjitter = .4f;
    private float timer;

    // Start is called before the first frame update
    public override void Start()
    {
        lastposition = transform.position;
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        timer += Time.deltaTime;
        if (timer > delay) {
            if (Vector3.Distance(lastposition, transform.position) > mindistance) {
                NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.small, "ee", (Vector2)transform.position + Random.insideUnitCircle * textjitter);
            }
            lastposition = transform.position;
            timer = 0;
        }
    }


    
}
