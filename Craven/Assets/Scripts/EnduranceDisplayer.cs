﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnduranceDisplayer : MonoBehaviour {
    
    private HeroData data;
    
    public Image BarImage;
    private RectTransform barRT;
    public Animator SlashAnimator;
    public Gradient ColorByPercent;
    public float ExhaustedAlpha = 0.4f;

    public FxPackageController FXNopeShake;

    private float maxWidth;

    public void Init(HeroData newData) {
        data = newData;
        barRT = BarImage.GetComponent<RectTransform>();
        maxWidth = barRT.rect.width;
    }

    public void DoNopeShake() {
        FXNopeShake.TriggerAll();
    }

    private void Update() {
        if (data != null) {
            BarImage.color = ColorByPercent.Evaluate(data.Endurance);
            SlashAnimator.speed = data.Endurance < data.EnduranceCostCharge ? 2f : 1f;
            if (data.Endurance < data.EnduranceCostCharge) {
                SlashAnimator.speed = 2f;
                BarImage.color = new Color(BarImage.color.r, BarImage.color.g, BarImage.color.b, ExhaustedAlpha);
            } else {
                SlashAnimator.speed = 1f;
            }
            barRT.sizeDelta = new Vector2(Mathf.Lerp(-maxWidth, 0f, data.Endurance), barRT.sizeDelta.y);
        }
    }
    
}
