﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
#if UNITY_EDITOR 
namespace UnityEditor {
    /// <summary>
    /// Randomly selects a tile to paint.
    /// <para>Adapted from https://github.com/Unity-Technologies/2d-techdemos.</para>
    /// </summary>
    [CustomGridBrush(false, false, false, "Prefab Plus")]
    public class PrefabPainterPlus : UnityEditor.Tilemaps.GridBrush {
        /// <summary>
        /// Prefab to create.
        /// </summary>
        public GameObject Prefab;



        /// <summary>
        /// Disables flood fill.
        /// </summary>
        /// <param name="gridLayout"></param>
        /// <param name="brushTarget"></param>
        /// <param name="position"></param>
        public override void FloodFill(GridLayout gridLayout, GameObject brushTarget, Vector3Int position) {
            return;
        }

        /// <summary>
        /// Paint spawn tile.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="brushTarget"></param>
        /// <param name="position"></param>
        public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position) {
            Erase(grid, brushTarget, position);
            Tilemap tilemap = brushTarget.GetComponent<Tilemap>();
            GameObject gameObject = GameObject.Instantiate(Prefab);
            gameObject.transform.SetParent(brushTarget.transform, false);
            //this new vector3 we add is half of the tile size, that places the tile directly in the middle of the new scene
            gameObject.transform.position = tilemap.CellToWorld(position) + new Vector3(.5f, .5f, 0);

            Undo.RegisterCreatedObjectUndo(gameObject, "Instantiated prefab");
        }

        /// <summary>
        /// Removes instantiated objects at position.
        /// </summary>
        /// <param name="gridLayout"></param>
        /// <param name="brushTarget"></param>
        /// <param name="position"></param>
        public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position) {
            Tilemap tilemap = brushTarget.GetComponent<Tilemap>();

            foreach (Transform childTransform in brushTarget.transform) {
                if (tilemap.WorldToCell(childTransform.position) == position) {
                    //GameObject.DestroyImmediate(childTransform.gameObject);
                    Undo.DestroyObjectImmediate(childTransform.gameObject);
                }
            }
        }
        public override void Rotate(RotationDirection direction, GridLayout.CellLayout layout) {
            if (direction == RotationDirection.Clockwise) {
                Prefab.transform.Rotate(Prefab.transform.forward, 90f);
            }
            else {
                Prefab.transform.Rotate(Prefab.transform.forward, -90f);
            }

        }

        /// <summary>
        /// Create new random brush asset.
        /// </summary>
        [MenuItem("Assets/Create/Prefab Plus")]
        public static void CreateBrush() {
            string path = EditorUtility.SaveFilePanelInProject("Save Prefab Plus", "New Prefab Plus", "asset", "Save Prefab Plus", "Assets");

            if (path == "")
                return;

            AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<PrefabPainterPlus>(), path);
        }
    }
    [CustomEditor(typeof(PrefabPainterPlus))]
    public class PrefabBrushEditor : UnityEditor.Tilemaps.GridBrushEditor {
        private PrefabPainterPlus prefabBrush { get { return target as PrefabPainterPlus; } }

        GameObject holder;

        public override void OnMouseLeave() {
            base.OnMouseLeave();

            if (holder)
                DestroyImmediate(holder);
        }


        public override void OnPaintSceneGUI(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing) {

            base.OnPaintSceneGUI(gridLayout, null, position, tool, executing);


                holder = prefabBrush.Prefab;

            holder.transform.position = position.position + new Vector3(.5f, 0.5f, 0.0f);

        }

    }
}
#endif