﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camshake : MonoBehaviour {
	
    public static Camshake Instance;

    public bool Test;
    public float TestTrauma;
    public float DecayRate = 0.5f;
    public float PerlinSpeed = 10f;
    public float ShakeMax;
    public float TranslationMultiplier = 0.01f;
    public float RotationMultiplier = 10f;
    public FxApplierTranslate TranslationApplier;
    public FxApplierRotate RotationApplier;

    private Vector2 RootPosition;

    private float curTrauma = 0f;
	

    private void Awake() {
        Instance = this;
        RootPosition = transform.position;
        TranslationApplier.SetUp(gameObject);
        RotationApplier.SetUp(gameObject);
    }

    private void Update() {

        if (Test) {
            Test = false;
            AddTrauma(TestTrauma);
        }
		
    }

    public void AddTrauma(float trauma) {
        TranslationApplier.SetShakeFactor(-1, trauma * ShakeMax * TranslationMultiplier, PerlinSpeed, DecayRate);
        RotationApplier.SetShakeFactor(-1, trauma * ShakeMax * RotationMultiplier, PerlinSpeed, DecayRate);
    }
	
}
