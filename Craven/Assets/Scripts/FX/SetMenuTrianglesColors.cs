﻿using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Sets the list of colors for a material using the MenuTriangles shader.
/// If color changes don't update in the editor, save the scene or reload it and it should reset
/// </summary>
[ExecuteInEditMode]
public class SetMenuTrianglesColors : MonoBehaviour {

	public float Angle;
	public Color[] Colors;


	private void Awake() {
		SetTheColors(new Color[16]); // this ends up being the maximum length of the array in the shader
		SetTheColors(Colors);
	}

	#if UNITY_EDITOR
	private void Update() {
		SetTheColors(Colors);
	}
	#endif

	private void SetTheColors(Color[] colors) {
		if (colors == null || colors.Length <= 0) return;
		Image image = GetComponent<Image>();
		if (image == null) return;
		if (image.material == null) return;
		
		image.material.SetInt("_ColorsLength", colors.Length);
		image.material.SetColorArray("_Colors", colors);
		image.material.SetFloat("_Angle", Angle);
	}
}
