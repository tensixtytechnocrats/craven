﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class FxLightingManager : FxApplier {

	public static FxLightingManager Instance;

	public Image OverImage;
	public Image UnderImage;

	void Awake () {

		if (Instance == null) {
			Instance = this;
		}

	}

	public void SetLightingFactor (int sfxId, Color newColor, Color newUnderColor, float newDecayRate, AnimationCurve newEaseInCurve) {

		FxFactorLighting factor = (FxFactorLighting)FactorWithId(sfxId);
		if(factor != null) {
			factor.Value = newColor.a;
			factor.UnderValue = newUnderColor.a;
		} else {
			factor = new FxFactorLighting ();
			factor.SfxId = sfxId;
			factor.Value = newColor.a;
			factor.UnderValue = newUnderColor.a;
			factor.OverColor = newColor;
			factor.UnderColor = newUnderColor;
			factor.DecayRate = newDecayRate;
			factor.EaseInCurve = newEaseInCurve;
			ActiveFactors.Add(factor);
		}

	}

	void Update () {

		ApplyLighting();
		AgeFactors();

	}

	protected void ApplyLighting () {

		if (OverImage != null) OverImage.color = FinalColor();
		if (UnderImage != null) UnderImage.color = FinalColor(true);

	}

	protected Color FinalColor (bool underColor = false) {

		Color color = MergedColor(underColor);
		float totalVal = TotalValue(underColor);
		return new Color (color.r, color.g, color.b, totalVal);

	}

	protected Color MergedColor (bool underColor = false) {
		Vector3 colorVec = new Vector3 (0F, 0F, 0F);
		float valueDenominator = 0F;
		foreach (var fxFactor in ActiveFactors) {
			var factor = (FxFactorLighting) fxFactor;
			float factorValue = underColor ? factor.ProcessedUnderValue() : factor.ProcessedValue();
			colorVec.x += (underColor ? factor.UnderColor : factor.OverColor).r * factorValue;
			colorVec.y += (underColor ? factor.UnderColor : factor.OverColor).g * factorValue;
			colorVec.z += (underColor ? factor.UnderColor : factor.OverColor).b * factorValue;
			valueDenominator += factorValue;
		}
		if (Math.Abs(valueDenominator) < 0.001f)
			return Color.black;
		colorVec /= valueDenominator;
		return new Color (colorVec.x, colorVec.y, colorVec.z);
	}

	protected float TotalValue (bool underValue = false) {
		float p = 0F;
		foreach (var fxFactor in ActiveFactors) {
			var factor = (FxFactorLighting) fxFactor;
			p += (underValue ? factor.ProcessedUnderValue() : factor.ProcessedValue()) * (1F - p);
		}
		return p;
	}

}

public class FxFactorLighting : FxFactor {

	public Color OverColor;
	public Color UnderColor;
	public float UnderValue;

	public AnimationCurve EaseInCurve;

	public override void UpdateFactor () {

		Age += Time.deltaTime;
		DecayValue();
		if (Value > DeletionThreshold || UnderValue > DeletionThreshold)
			LastAgeAboveDeletionThreshold = Age;
		if (Age - TimeBeforeDeletion > LastAgeAboveDeletionThreshold)
			ShouldBeDeleted = true;

	}

	public override float ProcessedValue () {

		return Mathf.Clamp01(Value) * EaseInCurve.Evaluate(Age);

	}

	public float ProcessedUnderValue () {

		return Mathf.Clamp01(UnderValue) * EaseInCurve.Evaluate(Age);

	}

	protected override void DecayValue () {

		Value = Value * Mathf.Exp(-DecayRate * Time.deltaTime);
		UnderValue = UnderValue * Mathf.Exp(-DecayRate * Time.deltaTime);

	}

}
