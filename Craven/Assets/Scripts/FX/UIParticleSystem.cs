﻿using UnityEngine;

/// <summary>
/// A system to display particles in the UI
/// </summary>
[ExecuteInEditMode]
public class UIParticleSystem : MonoBehaviour {

	public ParticleSystem ParticleSystem;
	public RectTransform ParticlePrefab;
	private RectTransform[] pool;
	private bool didStop;
	private ParticleSystem.Particle[] particles;
	private int highestParticleCount = -1;
	
	public Vector3 ParticlePositionScale = Vector3.one;
	public bool FlipZandY = true;


	private void InitializeIfNeeded() {
		if (ParticleSystem == null) return;
		if(pool == null) pool = new RectTransform[ParticleSystem.main.maxParticles];
		if(particles == null) particles = new ParticleSystem.Particle[ParticleSystem.main.maxParticles];
	}

	private void Update() {
		if (ParticleSystem == null || ParticlePrefab == null) return;
		if (ParticleSystem.isPaused) return;
		if (!didStop && ParticleSystem.isStopped) {
			didStop = true;
			TurnParticlesOff();
		}

		if (ParticleSystem.isPlaying) {
			didStop = false;
			SetParticles();
		}
	}

	private RectTransform NewParticle() {
		RectTransform rt = Instantiate(ParticlePrefab);
		rt.gameObject.hideFlags = HideFlags.HideAndDontSave;
		rt.SetParent(transform);
		return rt;
	}

	private void TurnParticlesOff() {
		for (int i = 0; i < highestParticleCount; i++) {
			if(pool[i] != null) pool[i].gameObject.SetActive(false);
		}
	}

	private void SetParticles() {
		InitializeIfNeeded();
		
		int particleCount = ParticleSystem.GetParticles(particles);
		highestParticleCount = highestParticleCount > particleCount ? highestParticleCount : particleCount;
		for (int i = 0; i < particleCount; i++) {
			if (pool[i] == null) pool[i] = NewParticle();
			pool[i].gameObject.SetActive(true);
			Vector3 particlePos = Vector3.Scale(particles[i].position, ParticlePositionScale);
			if (FlipZandY) {
				float temp = particlePos.z;
				particlePos.z = particlePos.y;
				particlePos.y = temp;
			}

			pool[i].position = transform.position + particlePos;
			pool[i].eulerAngles = particles[i].rotation3D;
			pool[i].localScale = particles[i].GetCurrentSize3D(ParticleSystem);
		}

		for (int i = particleCount; i < highestParticleCount; i++) {
			if(pool[i] != null) pool[i].gameObject.SetActive(false);
		}
	}
}
