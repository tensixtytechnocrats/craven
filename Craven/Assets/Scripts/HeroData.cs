﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class HeroData {

	public string FirstName;
	public string LastName;
	public string Name { get {
		return FirstName + " " + LastName;
	} } 
	public HeroDataDisplayer Displayer;
	public Player RewiredPlayer;
	public int PlayerNumber;
	public HeroTemplate Template;
	public Transform SpawnPoint;
	public GameObject Object;
	public HeroController Controller;
	public float CurRumble;
	
	// Score
	public int KillCount;

	public bool HasTorch;
	public float SecondsWithTorch;

	public float Score;
	public void AddKill() { KillCount++; Score += ScoreManager.Instance.ScoreData.pointsPerKill; }
	public void AddTimeWithTorch(float seconds) { SecondsWithTorch += seconds; Score += ScoreManager.Instance.ScoreData.pointsPerTorchSecond * seconds; }
	
	// Respawn
	public bool IsDead;
	public float SecondsUntilRespawn;
	
	// Rock Ammo
	public RockAmmoDisplayer RockAmmoDisplayer;
	public float RockAmmoMax = 3f;
	public float RockAmmo = 3f;
	public float RockRegen = 0.2222f;
	
	// Endurance
	public EnduranceDisplayer EnduranceDisplayer;
	public float EnduranceMax = 1f;
	public float Endurance = 1f;
	public float EnduranceRegen {get {return HasTorch ? 0.06f : 0.12f; }}
	public float EnduranceCostRock = 0.1f;
	public float EnduranceCostRun = 0.18f;
	public float EnduranceCostCharge = 0.5f;

	public void AddRumble(float amount) { CurRumble = Mathf.Clamp01(CurRumble + amount); }

}
