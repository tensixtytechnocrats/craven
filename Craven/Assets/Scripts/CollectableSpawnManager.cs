using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CollectableSpawnManager : MonoBehaviour {

    public GameObject CollectablePrefab;

    public GameObject CollectableSpawnPoints;
    public AnimationCurve SpeedOfCollectablesByScoreOfTorchHolder;
    public float EnergyNeededPerCollectable;
    
    private GameObject currentCollectable;
    private float currentEnergy;

    private void Update() {
        HeroData heroWithTorch = HeroManager.Instance.GetHeroWithTorch();
        if (heroWithTorch == null) return;
        if (currentCollectable != null) return;
        currentEnergy += Time.deltaTime * SpeedOfCollectablesByScoreOfTorchHolder.Evaluate(heroWithTorch.Score);
        if (currentEnergy > EnergyNeededPerCollectable) {
            currentEnergy = 0f;
            SpawnCollectable();
        }
    }

    private void SpawnCollectable() {
        List<(Transform t, float dist)> points = new List<(Transform t, float dist)>();
        foreach (Transform sp in CollectableSpawnPoints.GetComponentsInChildren<Transform>()) {
            float bestDistSoFar = 10000f;
            foreach (HeroData data in HeroManager.Instance.Datas) {
                if (data.Object == null) continue;
                float heroDistance = Vector2.Distance(sp.position, data.Object.transform.position);
                bestDistSoFar = Mathf.Min(heroDistance, bestDistSoFar);
            }
            points.Add((sp, bestDistSoFar));
        }

        points = points.OrderByDescending(e => e.dist).ToList();
        Transform chosenPoint = points[Random.Range(0, 5)].t;
        currentCollectable = Instantiate(CollectablePrefab, chosenPoint.position, Quaternion.identity, null);
    }
    
}
