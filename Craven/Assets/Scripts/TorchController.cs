﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class TorchController : MonoBehaviour {

	public static TorchController Instance;
	
	public RectTransform LightTransform;
	private Vector2 LightSizeVector;
	private Vector2 StartSizeDelta;
	private Vector2 EndSizeDelta;
	private bool isBeingCarried = false;
	
	[Header("Anticipation")]
	public float Ant_AnticipateTime = 8f;
	public AnimationCurve Ant_TextOpac;
	public AnimationCurve Ant_TextCompletion;
	public AnimationCurve Ant_LightSizePercent;
	public ParticleSystem RippleIn;
	
	// Text
	public TextMeshPro Ant_Text;
	private int finalCharacters = 0;

	private bool Ant_IsAnticipating = true;
	private float Ant_timer;
	private float Ant_Progress { get { return Ant_timer / Ant_AnticipateTime; } }
	private float Ant_timeLeft { get { return Ant_AnticipateTime - Ant_timer; } }

	[Header("Reveal")]
	public ParticleSystem RippleOut;
	public SpriteRenderer TorchSprite;
	public FxPackageController FXReveal;
	public FxPackageController FXPickupShrink;
	public GameObject PickupZone;
	public SoundCall RevealSound;
	public PPFactor PPFactor;

	[Header("Light")]
	public SFLight FieldLight;
	public SFLight HeldLight;

	public float HeldLightTransitionTime;
	private float heldLightTransitionTimer;
	private const float fieldLightIntensity = 1f;
	private const float heldLightIntensity = 0.75f;
	
	private string AnticipationString {
		get {
			StringBuilder sb = new StringBuilder("Torch appears in ");
			sb.Append((int) Ant_timeLeft + 1);
			float spareTime = Ant_timeLeft % 1f;
			int elipsisDots = (int)((1f - spareTime) / 0.25f);
			for (int i = 0; i < elipsisDots; i++) { sb.Append("."); }
			return sb.ToString();
		}
	}

	private void Awake() {
		Instance = this;
		finalCharacters = AnticipationString.Length;
		EndSizeDelta = LightTransform.sizeDelta;
		LightTransform.sizeDelta -= new Vector2(LightTransform.rect.width, LightTransform.rect.height);
		StartSizeDelta = LightTransform.sizeDelta;
		TorchSprite.color = Color.clear;
	}
	
	private void Update() {
		
		// Anticipation
		if (Ant_IsAnticipating) {
			Ant_timer += Time.deltaTime;
			Ant_Text.color = new Color(1f, 1f, 1f, Ant_TextOpac.Evaluate(Ant_Progress));
			Ant_Text.maxVisibleCharacters = (int)(finalCharacters * Ant_TextCompletion.Evaluate(Ant_Progress));
			Ant_Text.text = AnticipationString;

			if (Ant_Progress >= 1f) {
				Ant_IsAnticipating = false;
				AllowPickup();
			}

			LightTransform.sizeDelta =
				Vector2.Lerp(StartSizeDelta, EndSizeDelta, Ant_LightSizePercent.Evaluate(Ant_Progress));
		}

		if (isBeingCarried) {
			heldLightTransitionTimer += Time.deltaTime;
			FieldLight.intensity = Mathf.Lerp(0f, fieldLightIntensity,
				1f - (heldLightTransitionTimer / HeldLightTransitionTime));
			HeldLight.intensity = Mathf.Lerp(0f, heldLightIntensity,
				heldLightTransitionTimer / HeldLightTransitionTime);
		}
		
	}

	private void AllowPickup() {
		SoundManager.Instance.PlaySound(RevealSound, gameObject);
		RippleIn.Stop();
		RippleOut.Play();
		TorchSprite.color = Color.white;
		FXReveal.TriggerAll();
		PPManager.Instance.AddFactor(PPFactor);
		PickupZone.SetActive(true);
	}

	public void PickupCollision(Collider2D other) {
		if (!isBeingCarried && other.tag == "Hero") {
			Transform curHighestTransform = other.transform;
			while (curHighestTransform != null && curHighestTransform.parent != null && curHighestTransform.parent.tag == "Hero") {
				curHighestTransform = curHighestTransform.parent;
			}
			curHighestTransform.GetComponent<HeroController>().PickupTorch();
			//FXPickupShrink.TriggerAll();
			isBeingCarried = true;
		}
	}

}
