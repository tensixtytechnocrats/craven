﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaggerController : MonoBehaviour {

	public enum ReportType {
		Kill,
		Stun
	};

	public ReportType Type;
	
	public HeroController HeroController;
	
	private void OnCollisionEnter2D(Collision2D other) {
		HeroController.DaggerCollision(other.collider, Type);
	}
	
	private void OnTriggerEnter2D(Collider2D other) {
		HeroController.DaggerCollision(other, Type);
	}
	
}
