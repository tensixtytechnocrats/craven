﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class NoiseManager : MonoBehaviour {

	public static NoiseManager Instance;
	
	public enum NoiseSize {
		tiny,
		small,
		medium,
		large
	}

	public GameObject tinyPrefab;
	public GameObject smallPrefab;
	public GameObject mediumPrefab;
	public GameObject largePrefab;

	private void Awake() { Instance = this; }

	public void MakeNoiseAtPoint(NoiseSize size, string textString, Vector2 position) {
		GameObject newNoise = Instantiate(PrefabFromSize(size), position, Quaternion.identity, transform);
		newNoise.GetComponent<NoiseController>().Init(textString);
	}

	public void AttachNoiseToObject(NoiseSize size, string textString, GameObject obj) {
		GameObject newNoise = Instantiate(PrefabFromSize(size), obj.transform.position, Quaternion.identity, obj.transform);
		newNoise.GetComponent<NoiseController>().Init(textString);
	}
	
	private GameObject PrefabFromSize(NoiseSize size) {
		switch (size) {
			case NoiseSize.tiny:
				return tinyPrefab;
			case NoiseSize.small:
				return smallPrefab;
			case NoiseSize.medium:
				return mediumPrefab;
			case NoiseSize.large:
				return largePrefab;
			default:
				return tinyPrefab;
		}
	}
	
}
