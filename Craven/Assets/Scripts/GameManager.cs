﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public AnimationCurve brightnessByGameProgress; //How bright the game should be at a given score
    public float endScore = 100; //Score at which the game should end.
    private float startTime;
    private float gamePercentage;

    private List<HouseLights> ActiveLights = new List<HouseLights>();
    private int numLights = 0; //Total number of HouseLights on map
    private float maxIntensity = 0;
    private int dimmingLights = 0;
    public float dimFraction = 0.05f; //How much each light should dim in one go
    public float dimTime = 1f; //Time it takes for each light to dimm each pass

    public List<List<Window>> AllWindows = new List<List<Window>>();
    private float lightningTimer = 5; //At some point fix to be torch spawn time
    public AnimationCurve ligntingIntervalProfile;


    public float GetGamePercentage => gamePercentage;

    void Awake()
    {
        Instance = this;
    }


    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
        
    }

    // Update is called once per frame
    void Update()
    {
        //Find Player with max score
        float maxScore = 0;
        foreach(HeroData hero in HeroManager.Instance.Datas)
        {
            if (hero.Score > maxScore) maxScore = hero.Score;
        }

        //Dimm random lights to match current score progress
        gamePercentage = (maxScore/endScore);
        float currentIntensity = 0;
        foreach (HouseLights light in ActiveLights) currentIntensity += light.targetIntensity;
        if ( currentIntensity > maxIntensity * brightnessByGameProgress.Evaluate(gamePercentage))
        {
            ActiveLights[Random.Range(0, ActiveLights.Count)].Animate(dimFraction, dimTime);
        }

        lightningTimer -= Time.deltaTime;
         if (lightningTimer < 0)
         {
            int wall = Random.Range(0, AllWindows.Count);
            if (AllWindows.Count > 0)
            {

                foreach (Window individualWindow in AllWindows[wall])
                {
                    individualWindow.Animate();
                }

                AllWindows[wall][Random.Range(0, AllWindows[wall].Count)].MakeNoise(); 
            }
        
             lightningTimer = ligntingIntervalProfile.Evaluate(Random.Range(0, 1));
        
         }
        
    }

    public void addLight(HouseLights lights)
    {
        ActiveLights.Add(lights);
        numLights++;
        maxIntensity += lights.GetComponentInChildren<SFLight>().intensity;
    }

    public void removeLight(HouseLights lights)
    {
        ActiveLights.Remove(lights);
        
    }

    /// <summary>
    /// Generates list of lists containing windows. Each window gets placed into a sublist based on orentation
    /// </summary>
    /// <param name="window"></param>
    /// <returns> returns true if successfully added to a list</returns>
    public bool addWindow(Window window)
    {

         float orentation = window.transform.eulerAngles.z;
         foreach (List<Window> subList in AllWindows)
         {
             if (orentation == subList[0].transform.eulerAngles.z)
             {
                 subList.Add(window);
                 return true;
             }
         }
        
         //Orentation doesn't match previous windows so create a new sublist
         List<Window> temp = new List<Window>();
         temp.Add(window);
         AllWindows.Add(new List<Window>(temp));
        return true;
        return true;


    }
}
