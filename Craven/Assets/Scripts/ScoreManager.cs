﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager Instance;
    public ScoreData ScoreData;

    private void Awake() { Instance = this; }

    private void Update() {
        foreach (HeroData data in HeroManager.Instance.Datas) {
            
            // Do Torch Scoring
            if (data.HasTorch) {
                data.AddTimeWithTorch(Time.deltaTime);
            }
            
        }
    }

}
