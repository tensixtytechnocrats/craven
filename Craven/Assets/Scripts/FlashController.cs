﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class FlashController : MonoBehaviour {

	public SFLight Light;
	public RectTransform LightRT;
	public AnimationCurve IntensityCurve;
	public AnimationCurve SizeCurve;
	private Vector2 SizeDeltaStart;
	private Vector2 SizeDeltaEnd;
	public float Duration = 5f;
	private float timer;
	private float Progress { get { return timer / Duration; } }

	private void Awake() {
		Light.intensity = 0f;
		SizeDeltaEnd = LightRT.sizeDelta;
		LightRT.sizeDelta -= new Vector2(LightRT.rect.width, LightRT.rect.height);
		SizeDeltaStart = LightRT.sizeDelta;
	}
	
	private void Update() {
		timer += Time.deltaTime;
		Light.intensity = IntensityCurve.Evaluate(Progress);
		LightRT.sizeDelta = Vector2.LerpUnclamped(SizeDeltaStart, SizeDeltaEnd, SizeCurve.Evaluate(Progress));
		if (Progress >= 1f) {
			Destroy(gameObject);
		}
	}

	public void SetColor(Color color) { Light.color = color; }

}
