﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyRingColorController : MonoBehaviour {

	public SpriteRenderer Body;
	public List<SpriteRenderer> Rings = new List<SpriteRenderer>();
	public float darkStrength;

	private void Update() {
		Color bodyColor = Body.color;
		List <ColorWeight> cw = new List<ColorWeight>() {
			new ColorWeight(bodyColor, 1f),
			new ColorWeight(Color.black, darkStrength)
		};
		Color blendColor = ColorUtilities.BlendColors(cw);
		foreach (SpriteRenderer ring in Rings) {
			ring.color = blendColor;
		}
	}

}
