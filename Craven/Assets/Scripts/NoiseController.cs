﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class NoiseController : MonoBehaviour, IDetachable {

	public bool Testing = true;
	public string TestString = "drip";
	
	public GameObject TextObject;
	public TextMeshPro Text;
	private int finalCharacters = 0;
	
	public float Lifetime = 8f;
	public AnimationCurve TextOpac;
	public float ShakePower = 0.05f;
	public AnimationCurve TextShake;
	public AnimationCurve TextCompletion;

	private float timer;
	private float Progress { get { return timer / Lifetime; } }
	private Vector2 textRootPosition;

	private void Awake() {
		if (Testing) Init(TestString);
	}

	public void Init(string text) {
		Text.maxVisibleCharacters = 0;
		textRootPosition = TextObject.transform.localPosition;
		Text.text = text;
		Text.color = Color.clear;
		finalCharacters = Text.text.Length;
	}

	private void Update() {
		timer += Time.deltaTime;

		Text.color = new Color(1f, 1f, 1f, TextOpac.Evaluate(Progress));
		Text.maxVisibleCharacters = (int)(finalCharacters * TextCompletion.Evaluate(Progress));
		
		// Do Shake
		TextObject.transform.localPosition = textRootPosition + Random.insideUnitCircle * TextShake.Evaluate(Progress) * ShakePower;
		
		if (Progress >= 1f) {
			Destroy(gameObject);
		}
		
		transform.rotation = Quaternion.identity;
	}

	public void Detach() { transform.parent = null; }
	
}
