using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableController : MonoBehaviour {

    public float PointsGiven;

    public SoundCall AppearanceSound;
    
    public SFLight Light;
    public AnimationCurve LightCurveIn;
    public AnimationCurve LightCurveOut;

    public Transform SpritesTransform;
    
    public AnimationCurve SpritesPosYCurve;
    public AnimationCurve SpritesPosXCurve;

    public AnimationCurve SpritesScaleCurveIn;
    public AnimationCurve SpritesScaleCurveOut;

    public AnimationCurve SpritesRotateSpeedByYOffset;

    public ParticleSystem ParticleSystem;
    public AnimationCurve ParticleSimulationSpeedOut;

    public float OutTime;
    
    private float timeIn;
    private float timeOut;

    private bool hasBeenCollected;

    private void Start() {
        SoundManager.Instance.PlaySound(AppearanceSound, gameObject);
    }
    
    private void Update() {

        timeIn += Time.deltaTime;
        if (hasBeenCollected) {
            timeOut += Time.deltaTime;
        }

        Light.intensity = LightCurveIn.Evaluate(timeIn) * LightCurveOut.Evaluate(timeOut);
        SpritesTransform.localPosition = new Vector2(SpritesPosXCurve.Evaluate(timeIn), SpritesPosYCurve.Evaluate(timeIn));
        SpritesTransform.localScale = Vector3.one * SpritesScaleCurveIn.Evaluate(timeIn) * SpritesScaleCurveOut.Evaluate(timeOut);
        SpritesTransform.Rotate(0f, 0f, Time.deltaTime * SpritesRotateSpeedByYOffset.Evaluate(SpritesPosYCurve.Evaluate(timeIn)));
        var particleSystemMain = ParticleSystem.main;
        particleSystemMain.simulationSpeed = ParticleSimulationSpeedOut.Evaluate(timeOut);

        if (timeOut > OutTime) {
            Destroy(gameObject);
        }
        
    }
    
    private void OnTriggerEnter2D(Collider2D other) {
        CheckPickup(other);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        CheckPickup(other.collider);
    }

    private void CheckPickup (Collider2D other) {
        if (hasBeenCollected) return;
        if (other.tag != "Hero") return;
        Transform curHighestTransform = other.transform;
        while (curHighestTransform != null && curHighestTransform.parent != null && curHighestTransform.parent.tag == "Hero") {
            curHighestTransform = curHighestTransform.parent;
        }
        HeroController heroController = curHighestTransform.GetComponent<HeroController>();
        if (heroController == null) return;
        if (!heroController.HeroData.HasTorch) return;
        DoPickup(heroController.HeroData);
    }

    private void DoPickup(HeroData heroData) {
        hasBeenCollected = true;
        GetComponent<Collider2D>().enabled = false;
        heroData.Score += PointsGiven;
        ParticleSystem.Stop();
    }

}
