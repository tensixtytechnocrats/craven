﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockController : MonoBehaviour {

    public float Speed;
    public float SpinRate;
    public Transform SpriteTransform;
    public AnimationCurve SizeByTime;
    public Rigidbody2D Body;
    
    private Vector2 dir;
    private float timer;
    private HeroData ownerData;

    public void Init(Vector2 newDir, HeroData data) {
        dir = newDir.normalized;
        ownerData = data;
    }

    private void Update() {
        timer += Time.deltaTime;
        SpriteTransform.Rotate(0f,0f,SpinRate * Time.deltaTime);
        float scale = SizeByTime.Evaluate(timer);
        transform.localScale = new Vector3(scale, scale, scale);
        transform.Translate(dir * Speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D other) {
        DoCollision(other.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        DoCollision(other.gameObject);
    }

    private void DoCollision(GameObject other) {
        Debug.LogWarning("Rock collided with " + other.name);
        if (other.gameObject.tag == "Wall") {
            NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.tiny, "thud", transform.position);
            Die();
        }

        if (other.gameObject.tag == "Hero") {
            HeroController controller = other.GetComponent<HeroController>();
            if (controller != null && controller.HeroData != ownerData) {
                NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.tiny, "Ow!", transform.position);
                Die();
            }
        }
    }

    private void Die() { Destroy(gameObject); }

}
