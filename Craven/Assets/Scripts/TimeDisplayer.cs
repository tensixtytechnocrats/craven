﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using TMPro;
using UnityEngine;

public class TimeDisplayer : MonoBehaviour {

	public static TimeDisplayer Instance;

	public TextMeshProUGUI TimerText;

	private void Awake() { Instance = this; }

	public void SetTime(float secondsLeft) {
		SetTime((int)secondsLeft);
	}

	public void SetTime(int secondsLeft) {
		int spareSeconds = secondsLeft % 60;
		secondsLeft -= spareSeconds;
		int minutes = secondsLeft / 60;
		TimerText.text = minutes + ":" + spareSeconds.ToString("D2");
	}

}
