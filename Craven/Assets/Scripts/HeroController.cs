﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class HeroController : MonoBehaviour {

	[HideInInspector] public HeroData HeroData;
	[HideInInspector] public int playerNum;
	
	private Player player;

	private Rigidbody2D body;

	public float MoveSpeed = 2f;

	public SpriteRenderer BodySprite;
	
	private Vector2 lastAimVec = Vector2.up;

	[Header("Running")]
	public AnimationCurve RunSpeedMultiplier;
	public float RunDistancePerNoise = 0.4f;
	private Vector2 lastRunPosition = Vector2.negativeInfinity;
	private bool doingOngoingRun;
	private float ongoingRunTimer;
	private float timeLastOngoingRunEnded = Mathf.NegativeInfinity;
	private const float timeBetweenOngoingRuns = 0.6f;

	[Header("Attack")]
	public float AttackSpeedMultiplier = 2f;
	public Warcries Warcries;
	public float AttackDuration;
	[HideInInspector] public float attackTimer;
	[HideInInspector] public bool isAttacking;
	private Vector2 curAttackVector;
	public float StunDuration;
	private float stunTimer;
	private bool isStunned;
	public float WoozyDuration;
	public AnimationCurve WoozySpeedMultiplier;
	private float woozyTimer;
	private bool isWoozy;

	public AnimationCurve AttackSpeedByChargeUpTime;
	public AnimationCurve AttackDurationByChargeUpTime;
	public AnimationCurve AttackRumbleByChargeUpTime;
	public FxPackageController AttackChargeUpShakeFX;
	public FxShakeObject AttackChargeUpShakeComponent;
	public AnimationCurve AttackShakeByChargeUpTime;
	public float MoveSpeedMultiplierWhileAttackChargingUp;
	private float attackChargeUpTime;
	private bool attackIsChargingUp;
	private float lastLockedInAttackDurationMultiplier;
	private float lastLockedInAttackSpeedMultiplier;

	[Header("Rock Throw")]
	public GameObject RockPrefab;
	
	[Header("Torch")]
	public float TorchRunMultiplier = 0.6f;

	[Header("Death")]
	public GameObject DeathSFXPrefab;

	public void Init(HeroData heroData) {
		playerNum = heroData.PlayerNumber;
		HeroData = heroData;
		
		player = HeroData.RewiredPlayer;
		body = GetComponent<Rigidbody2D>();
		lastAimVec = (HeroManager.Instance.transform.position - transform.position).normalized;
		HeroData.AddRumble(1f);
		SoundManager.Play(SoundManager.Common("Revive"), gameObject);
		FlashManager.Instance.SpawnFlash("Slam Flash", transform.position, HeroData.Template.LightColor);
	}

	private void Update() {
		float moveX = player.GetAxis("Move X");
		float moveY = player.GetAxis("Move Y");
		float aimX = player.GetAxis("Aim X");
		float aimY = player.GetAxis("Aim Y");
		Vector2 aimVec = ((aimX != 0f || aimY != 0f) ? new Vector2(aimX, aimY) :
			(moveX != 0f || moveY != 0f) ? new Vector2(moveX, moveY) :
			lastAimVec);
		lastAimVec = aimVec;
		float aimDeg = MathUtilities.degreesToLookAlongVector(aimVec);
		Vector2 moveVec = !isAttacking ? new Vector2(moveX, moveY) : curAttackVector;

        if (player.GetButton("Menu"))
        {
			UnityEngine.SceneManagement.SceneManager.LoadScene("Start Menu");
        }

		if (!isStunned) {
			
			// Handle Running State
			bool isRunning = player.GetButton("Run");
			if (doingOngoingRun && !isRunning) {
				doingOngoingRun = false;
				timeLastOngoingRunEnded = Time.time;
			}
			if (!doingOngoingRun && Time.time - timeBetweenOngoingRuns < timeLastOngoingRunEnded) {
				isRunning = false;
			}
			if (isRunning && HeroData.Endurance < HeroData.EnduranceCostRun * Time.deltaTime) {
				HeroData.EnduranceDisplayer.DoNopeShake();
				isRunning = doingOngoingRun = false;
				timeLastOngoingRunEnded = Time.time;
			}
			if (isRunning) {
				doingOngoingRun = true;
				ongoingRunTimer += Time.deltaTime;
			} else {
				ongoingRunTimer = 0f;
			}
			
			// Do Step Noise FX
			if (!isWoozy && (isRunning || isAttacking) && Vector2.Distance(transform.position, lastRunPosition) > RunDistancePerNoise) {
				HeroData.AddRumble(0.15f);
				lastRunPosition = transform.position;
				NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.tiny, "step", (Vector2)transform.position + Random.insideUnitCircle * 0.2f);
			}
			
			// Update Rock and Endurance Regen
			HeroData.RockAmmo = Mathf.Clamp(HeroData.RockAmmo + HeroData.RockRegen * Time.deltaTime, 0f, HeroData.RockAmmoMax);
			HeroData.Endurance = Mathf.Clamp(HeroData.Endurance + HeroData.EnduranceRegen * Time.deltaTime, 0f, HeroData.EnduranceMax);
			if (isRunning) HeroData.Endurance -= HeroData.EnduranceCostRun * Time.deltaTime;
			
			// Start New Attacks
			if (player.GetButtonDown("Attack") && !isAttacking && !isWoozy && !HeroData.HasTorch) {
				if (HeroData.Endurance >= HeroData.EnduranceCostCharge) {
					AttackChargeUpShakeFX.ToggleAll(true);
					attackIsChargingUp = true;
				} else {
					HeroData.EnduranceDisplayer.DoNopeShake();
				}
			}

			if (attackIsChargingUp) {
				attackChargeUpTime += Time.deltaTime;
				AttackChargeUpShakeComponent.ValueMultiplier = AttackShakeByChargeUpTime.Evaluate(attackChargeUpTime);
				HeroData.AddRumble(AttackRumbleByChargeUpTime.Evaluate(attackChargeUpTime) * Time.deltaTime);
			}

			if (player.GetButtonUp("Attack") && attackIsChargingUp) {
				HeroData.Endurance -= HeroData.EnduranceCostCharge;
				SoundManager.Instance.PlaySound(SoundManager.Instance.CommonSound("Charge"), gameObject);
				HeroData.AddRumble(0.5f);
				NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.medium, Warcries.Cries[Random.Range(0, Warcries.Cries.Count)], transform.position);
				isAttacking = true;
				attackTimer = 0f;
				curAttackVector = lastAimVec;
				lastLockedInAttackDurationMultiplier = AttackDurationByChargeUpTime.Evaluate(attackChargeUpTime);
				lastLockedInAttackSpeedMultiplier = AttackSpeedByChargeUpTime.Evaluate(attackChargeUpTime);
				attackIsChargingUp = false;
				attackChargeUpTime = 0f;
				AttackChargeUpShakeFX.ToggleAll(false);
			} 
			
			// Do Rock Throw
			if (player.GetButtonDown("Throw") && !isAttacking && !isWoozy && HeroData.RockAmmo >= 1f) {
				if (HeroData.Endurance >= HeroData.EnduranceCostRock) {
					HeroData.Endurance -= HeroData.EnduranceCostRock;
					ThrowRock();
				} else {
					HeroData.EnduranceDisplayer.DoNopeShake();
				}
			}

			// Set Speed
			float speed = MoveSpeed;
			if (isAttacking) speed *= AttackSpeedMultiplier * lastLockedInAttackSpeedMultiplier;
			if (attackIsChargingUp) speed *= MoveSpeedMultiplierWhileAttackChargingUp;
			else if (isRunning) speed *= RunSpeedMultiplier.Evaluate(ongoingRunTimer);
			if (isWoozy) speed *= WoozySpeedMultiplier.Evaluate(woozyTimer / WoozyDuration);
			if (HeroData.HasTorch) speed *= TorchRunMultiplier;

			// Execute Final Movement
			if (!isAttacking) {
				transform.rotation = Quaternion.identity;
				transform.Rotate(0f, 0f, aimDeg - 90f);
			}
			body.velocity = moveVec * speed;
		} 
		
		// Process Existing Attack
		if (isAttacking) {
			attackTimer += Time.deltaTime;
			if (attackTimer >= AttackDuration * lastLockedInAttackDurationMultiplier) {
				isAttacking = false;
			}
		}
		
		// Process Existing Stun
		if (isStunned) {
			body.velocity *= 0.9f;
			stunTimer += Time.deltaTime;
			if (stunTimer >= StunDuration) {
				isStunned = false;
				isWoozy = true;
				woozyTimer = 0f;
			}
		}
		
		// Process Existing Woozy
		if (isWoozy) {
			woozyTimer += Time.deltaTime;
			if (woozyTimer >= WoozyDuration) {
				isWoozy = false;
			}
		}
	}

	private void StartStun() {
		HeroData.AddRumble(1f);
		Camshake.Instance.AddTrauma(.5f);
		SoundManager.Play(SoundManager.Common("Wall Slam"), gameObject);
		NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.medium, "SLAM", transform.position);
		FlashManager.Instance.SpawnFlash("Slam Flash", transform.position, HeroData.Template.LightColor);
		isAttacking = false;
		stunTimer = 0f;
		isStunned = true;
	}

	private void Die() {
		foreach (IDetachable detachable in GetComponentsInChildren<IDetachable>()) {
			detachable.Detach();
		}

		Instantiate(DeathSFXPrefab, transform.position, Quaternion.identity, null).GetComponent<DeathSFX>().Init(HeroData);
		HeroData.IsDead = true;
		HeroData.HasTorch = false;
		HeroData.SecondsUntilRespawn = HeroManager.Instance.RespawnTime;
		HeroData.Score -= HeroData.Score * ScoreManager.Instance.ScoreData.percentStolenOnKill;
		Destroy(gameObject);
	}

	public void DaggerCollision (Collider2D other, DaggerController.ReportType type) {
		if (isAttacking) {
			if (other.tag == "Wall" && type == DaggerController.ReportType.Stun) {
				StartStun();
			} else if (other.tag == "Hero" && type == DaggerController.ReportType.Kill) {

				Transform curHighestTransform = other.transform;
				while (curHighestTransform != null && curHighestTransform.parent != null && curHighestTransform.parent.tag == "Hero") {
					curHighestTransform = curHighestTransform.parent;
				}
				HeroController otherController = curHighestTransform.GetComponent<HeroController>();
				
				if ((!otherController.isAttacking || otherController.attackTimer < attackTimer) && !otherController.HeroData.IsDead) {
					if (otherController.HeroData.HasTorch) {
						otherController.HeroData.HasTorch = false;
						PickupTorch();
					}

					HeroData.AddKill();
					HeroData.Score += otherController.HeroData.Score * ScoreManager.Instance.ScoreData.percentStolenOnKill;
					otherController.Die();
					SoundManager.Instance.PlaySound(SoundManager.Instance.CommonSound("Kill"), gameObject);
				}
			}
		}
	}
	
	// Called by TorchController
	public void PickupTorch() {
		HeroData.HasTorch = true;
		TorchController.Instance.transform.position = transform.position;
		TorchController.Instance.transform.rotation = transform.rotation;
		TorchController.Instance.transform.SetParent(transform);
	}

	private void ThrowRock() {
		HeroData.RockAmmo -= 1f;
		HeroData.AddRumble(0.5f);
		Instantiate(RockPrefab, transform.position, Quaternion.identity, null).GetComponent<RockController>().Init(lastAimVec, HeroData);
	}

	private void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.tag == "Wall") {
			HeroData.AddRumble(0.03f);
		}
	}

	private void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Wall") {
			HeroData.AddRumble(0.03f);
		}
	}
}
