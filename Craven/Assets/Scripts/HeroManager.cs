﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class HeroManager : MonoBehaviour {

	public static HeroManager Instance;
	
	private List<Player> rewiredPlayers = new List<Player>();
	private List<Player> inactiveRewiredPlayers = new List<Player>();
	
	public HeroNames HeroNames;
	private List<int> usedFirstNames = new List<int>();
	private List<int> usedLastNames = new List<int>();

	public GameObject HeroPrefab;
	[HideInInspector] public List<HeroData> Datas = new List<HeroData>();

	public GameObject Canvas;
	private List<HeroDataDisplayer> displayers = new List<HeroDataDisplayer>();
	private List<HeroDataDisplayer> unusedDisplayers = new List<HeroDataDisplayer>();
	
	public List<Transform> SpawnPoints = new List<Transform>();
	private List<Transform> UnusedSpawnPoints = new List<Transform>();

	public List<HeroTemplate> HeroTemplates = new List<HeroTemplate>();
	private List<HeroTemplate> UnusedTemplates = new List<HeroTemplate>();

	public float RespawnTime = 5f;

	private void Awake() {
		Instance = this;
		
		foreach (Player player in ReInput.players.Players) {
			rewiredPlayers.Add(player);
			inactiveRewiredPlayers.Add(player);
		}

		foreach (Transform t in SpawnPoints) {
			UnusedSpawnPoints.Add(t);
		}

		foreach (HeroDataDisplayer d in Canvas.GetComponentsInChildren<HeroDataDisplayer>()) {
			displayers.Add(d);
			unusedDisplayers.Add(d);
		}

		foreach (HeroTemplate h in HeroTemplates) {
			UnusedTemplates.Add(h);
		}
	}

	private void Update() {
		for (int i = inactiveRewiredPlayers.Count - 1; i >= 0; i--) {
			if (inactiveRewiredPlayers[i].GetButton("Control") && inactiveRewiredPlayers[i].GetButton("R")) {
				SceneManager.LoadScene("Playable Map");
			}
		}


		for (int i = inactiveRewiredPlayers.Count - 1; i >= 0; i--) {
			if (inactiveRewiredPlayers[i].GetButton("Start")) {
				SpawnPlayer(inactiveRewiredPlayers[i]);
			}
		}

		for (int i = 0; i < Datas.Count; i++) {
			if (Datas[i].IsDead) {
				Datas[i].SecondsUntilRespawn -= Time.deltaTime;
				if (Datas[i].SecondsUntilRespawn < 0f) {
					RespawnHero(Datas[i]);
				}
			}
		}


	}

	private void SpawnPlayer(Player rewiredPlayer) {
		
		// Rig up Hero Data
		inactiveRewiredPlayers.Remove(rewiredPlayer);
		HeroData newData = new HeroData();
		newData.FirstName = RandomUnusedFirstName();
		newData.LastName = RandomUnusedLastName();
		newData.Displayer = unusedDisplayers[0];
		unusedDisplayers.RemoveAt(0);
		newData.RewiredPlayer = rewiredPlayer;
		newData.PlayerNumber = rewiredPlayer.id;
		newData.Template = UnusedTemplates[0];
		UnusedTemplates.RemoveAt(0);
		newData.SpawnPoint = SpawnPoints[Datas.Count];
		
		// Instantiate Player
		newData.Object = Instantiate(HeroPrefab, newData.SpawnPoint.position, Quaternion.identity, null);
		newData.Controller = newData.Object.GetComponent<HeroController>();
		newData.Controller.BodySprite.sprite = newData.Template.CharacterSprite;
		newData.Controller.Init(newData);
		
		// Initialized Displayer 
		newData.Displayer.Init(newData);
		
		// Record Player
		Datas.Add(newData);

	}

	private void RespawnHero(HeroData data) {

		data.IsDead = false;
		
		// Instantiate Player
		data.SpawnPoint = FurthestSpawnPointFromPlayers();
		data.Object = Instantiate(HeroPrefab, data.SpawnPoint.position, Quaternion.identity, null);
		data.Controller = data.Object.GetComponent<HeroController>();
		data.Controller.BodySprite.sprite = data.Template.CharacterSprite;
		data.Controller.Init(data);
		
	}

	private string RandomUnusedFirstName() {
		List<string> names = new List<string>();
		for (int i = 0; i < HeroNames.FirstNames.Count; i++) {
			if (!usedFirstNames.Contains(i)) names.Add(HeroNames.FirstNames[i]);
		}
		int idx = UnityEngine.Random.Range(0, names.Count);
		usedFirstNames.Add(HeroNames.FirstNames.IndexOf(names[idx]));
		return names[idx];
	}
	
	private string RandomUnusedLastName() {
		List<string> names = new List<string>();
		for (int i = 0; i < HeroNames.LastNames.Count; i++) {
			if (!usedLastNames.Contains(i)) names.Add(HeroNames.LastNames[i]);
		}
		int idx = UnityEngine.Random.Range(0, names.Count);
		usedLastNames.Add(HeroNames.LastNames.IndexOf(names[idx]));
		return names[idx];
	}

	private Transform FurthestSpawnPointFromPlayers() {
		Transform bestPointSoFar = SpawnPoints[0];
		float longestMinDistSoFar = 0f;
		foreach (Transform sp in SpawnPoints) {
			float minDist = Mathf.Infinity;
			foreach (HeroData data in Datas) {
				if (data.Object != null && !data.IsDead) {
					if (Vector2.Distance(sp.position, data.Object.transform.position) < minDist) {
						minDist = Vector2.Distance(sp.position, data.Object.transform.position);
					}
				}
			}

			if (minDist > longestMinDistSoFar) {
				bestPointSoFar = sp;
				longestMinDistSoFar = minDist;
			}
		}

		return bestPointSoFar;
	}

	public HeroData GetHeroWithTorch() {
		foreach (HeroData heroData in Datas) {
			if (heroData.HasTorch) return heroData;
		}
		return null;
	}
	
}
