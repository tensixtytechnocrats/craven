﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseLights : Environmental
{
    private GameManager GM;
    public SFLight lightsource;

    public float deathTime = 4; //Time for light to fade out completly


    private float maxIntensity;
    public float targetIntensity;
    private float decayTime;
    private bool dimming;


    // Start is called before theirst frame update
    public override void Start()
    {
        GM = GameManager.Instance;
        GM.addLight(this);

        if(lightsource == null)
        {
            lightsource = GetComponentInChildren<SFLight>();
        }
        maxIntensity = lightsource.intensity;
        targetIntensity = maxIntensity;
        dimming = false;
   
    }

    public override void Update() {
        if (lightsource.intensity <= targetIntensity) dimming = false;
        if (dimming)
        {
            Animate();
        }

        if(lightsource.intensity <= 0)
        {
            GM.removeLight(this);
            lightsource.intensity = 0;
            dimming = false;
        }
    }

    /// <summary>
    /// Lowers light toward targetIntensity, step size dependent on set decayTime
    /// </summary>
    public override void Animate() {
        lightsource.intensity -= (lightsource.intensity - targetIntensity) * Time.deltaTime / decayTime;
        decayTime -= Time.deltaTime;
        return;
    }
    /// <summary>
    /// Starts light dimming by a fixed percent
    /// </summary>
    /// <param name="fraction">What percent of the max intensity the light should dim by</param>
    /// <param name="time">The time it should take to dim to the desired value</param>
    public void Animate(float fraction, float time)
    {
        targetIntensity -= (maxIntensity * fraction);
        decayTime = time;
        dimming = true;
    }

    public override bool Smash()
    {
        dimming = true;
        targetIntensity = 0f;
        decayTime = deathTime;
        return false;
    }

    public bool isDimming()
    {
        return dimming;
    }
}
