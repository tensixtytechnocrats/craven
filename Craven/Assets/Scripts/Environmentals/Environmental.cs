﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environmental : MonoBehaviour
{
    // Start is called before the first frame update
    public virtual void Start()
    {
        
    }

    // Update is called once per frame
    public virtual void Update()
    {
        
    }
    /// <summary>
    /// MakeNoise will use Noisemanager, and create a ottomatapea(AKA Noise)
    /// </summary>
    /// <returns>false if silent</returns>

    public virtual bool MakeNoise() {
        return false;
    }

    /// <summary>
    /// Animate includes lights, not just cool effects
    /// </summary>
    /// <returns>false if no animation</returns>
    public virtual void Animate() {
        return;
    }
    /// <summary>
    /// If the environmental is destructable, it will make a destruction specific noise among other things
    /// </summary>
    /// <returns>true if destroyed</returns>
    public virtual bool Smash() {
        return false;
    }



}
