﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class HeroDataDisplayer : MonoBehaviour {

	public CanvasGroup CanvasGroup;
	[FormerlySerializedAs("KillText")] public TextMeshProUGUI ScoreText;
	
	private HeroData heroData;

	public Image BackgroundImage;
	public RockAmmoDisplayer RockAmmoDisplayer;
	public EnduranceDisplayer EnduranceDisplayer;
	
	public AnimationCurve FadeInCurve;
	public AnimationCurve FadeOutCurve;
	private bool fadingIn = false;
	private bool fadingOut = false;
	private float timerIn;
	private float timerOut;
	private bool active = false;

	private string ScoreString {
		get {
			if (heroData.IsDead) {
				return "Respawn in: " + (int) heroData.SecondsUntilRespawn + "s";
			} else {
				ScoreData s = ScoreManager.Instance.ScoreData;
				float score = heroData.Score;
				score = score / s.pointsToWin;
				return (int) (score * 100) + "%";
			}
		}
	}

	public void Init(HeroData data) {
		heroData = data;
		data.RockAmmoDisplayer = RockAmmoDisplayer;
		RockAmmoDisplayer.Init(data);
		data.EnduranceDisplayer = EnduranceDisplayer;
		EnduranceDisplayer.Init(data);
		BackgroundImage.color = data.Template.GUIColor;
		
		active = true;
		timerIn = 0f;
		timerOut = 0f;
		fadingIn = true;
		fadingOut = false;
	}

	private void Update() {
		
		if (fadingIn) {
			timerIn += Time.deltaTime;
			if (timerIn > FadeInCurve.keys[FadeInCurve.keys.Length - 1].time) {
				fadingIn = false;
			}
		}

		if (fadingOut) {
			timerOut += Time.deltaTime;
			if (timerOut > FadeInCurve.keys[FadeOutCurve.keys.Length - 1].time) {
				fadingOut = false;
				active = false;
			}
		}

		CanvasGroup.alpha = (active ? 1f : 0f) * FadeInCurve.Evaluate(timerIn) * FadeOutCurve.Evaluate(timerOut);

		if (active) {
			ScoreText.text = ScoreString;
		}

	}

	public void FadeOut() {
		fadingIn = false;
		fadingOut = true;
	}

}
