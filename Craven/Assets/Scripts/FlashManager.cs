﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FlashManager : MonoBehaviour {

	public static FlashManager Instance;
	public List<GameObject> FlashPrefabs = new List<GameObject>();

	private void Awake() { Instance = this; }
	
	public void SpawnFlash(string flashName, Vector2 flashPosition, Color color = default(Color)) {
		GameObject prefab = FlashPrefabs.Where(e => e.name == flashName).First();
		FlashController controller = Instantiate(prefab, flashPosition, Quaternion.identity, null).GetComponent<FlashController>();
		if (color != default(Color)) {
			controller.SetColor(color);
		}
	}

}
