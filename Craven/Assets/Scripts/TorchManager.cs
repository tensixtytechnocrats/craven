﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchManager : MonoBehaviour {
	
	public List<Transform> TorchSpawnPoints = new List<Transform>();
	public float TorchSpawnTime = 10f;
	public GameObject TorchPrefab;
	private float torchSpawnTimer;
	private bool hasSpawned = false;

	private void Update() {
		if (!hasSpawned) {
			torchSpawnTimer += Time.deltaTime;
			if (torchSpawnTimer > TorchSpawnTime) {
				hasSpawned = true;
				Transform spawnPoint = TorchSpawnPoints[Random.Range(0, TorchSpawnPoints.Count)];
				Instantiate(TorchPrefab, spawnPoint.transform.position, Quaternion.identity, null);
			}
 		}
	}

}
