﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchPickupReporter : MonoBehaviour {

	public TorchController TorchController;

	private void OnTriggerEnter2D(Collider2D other) {
		TorchController.PickupCollision(other);
	}

	private void OnCollisionEnter2D(Collision2D other) {
		TorchController.PickupCollision(other.collider);
	}
}
