﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RockAmmoDisplayer : MonoBehaviour {

    private HeroData data;
    private List<RockSlotDisplayer> slots = new List<RockSlotDisplayer>();

    public void Init(HeroData newData) {
        data = newData;
        slots = GetComponentsInChildren<RockSlotDisplayer>().ToList();
    }
    
    private void Update() {
        if (data != null) {
            for (int i = 0; i < slots.Count; i++) {
                slots[i].SetPercent(Mathf.Clamp01(data.RockAmmo - i));
            }
        }
    }
    
}
