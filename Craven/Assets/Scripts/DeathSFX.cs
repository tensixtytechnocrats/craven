﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathSFX : MonoBehaviour {

    public FxPackageController FX;
    public float CameraTrauma;
    public PPFactor PPFactor;
    public float Duration;
    private float timer;

    public void Init(HeroData data) {
        foreach (ParticleSystem ps in GetComponentsInChildren<ParticleSystem>()) {
            ParticleSystem.MainModule main = ps.main;
            main.startColor = data.Template.LightColor;
            ps.Play();
        }
        PPManager.Instance.AddFactor(PPFactor);
        Camshake.Instance.AddTrauma(CameraTrauma);
    }

    private void Update() {
        timer += Time.deltaTime;
        if (timer > Duration) {
            Destroy(gameObject);
        }
    }

}
