﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileTest : MonoBehaviour
{
    public Tilemap tilegrid;
    public TileBase currentTile;
    public TileData whatiwant;

    // Start is called before the first frame update
    void Start()
    {
                 
    }

    // Update is called once per frame
    void Update()
    {
        currentTile = tilegrid.GetTile(ConvertToVector3(transform.position));
        //currentTile.GetTileData(ConvertToVector3(transform.position), tilegrid.GetComponent<Tilemap>(), ref whatiwant);
    }

    public static Vector3Int ConvertToVector3(Vector3 vec3) {
        return new Vector3Int((int)vec3.x, (int)vec3.y, (int)vec3.z);
    }
}
