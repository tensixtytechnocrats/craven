﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RumbleManager : MonoBehaviour {

	public static RumbleManager Instance;

	private void Awake() { Instance = this; }

	private void Update() {
		foreach (HeroData data in HeroManager.Instance.Datas) {
			data.RewiredPlayer.SetVibration(0, data.CurRumble);
			data.RewiredPlayer.SetVibration(1, data.CurRumble);
			data.CurRumble = MathUtilities.Decay(data.CurRumble, Time.deltaTime, 10f);
		}
	}

}
