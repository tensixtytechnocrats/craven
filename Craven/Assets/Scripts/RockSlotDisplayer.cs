﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSlotDisplayer : MonoBehaviour {

    public Animator Animator;
    public Transform MaskTransform;

    private bool isReady = true;

    public void SetPercent(float percent) {
        if (percent >= 1f &&!isReady) {
            Animator.Play("Rock Slot Flash");
        }
        isReady = percent >= 1f;
        MaskTransform.localScale = new Vector2(1f, isReady ? 1f : Mathf.Lerp(.15f, 0.8f, percent));
    }
    
}
