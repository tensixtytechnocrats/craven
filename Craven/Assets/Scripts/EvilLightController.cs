﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilLightController : MonoBehaviour {

    public SpriteRenderer SpriteCrisp;
    public SpriteRenderer SpriteBlurred;

    public Color StartColor;
    public Color EndColor;

    public float StartSpeed;
    public float EndSpeed;

    public float BlurredBlackPortion;

    public AnimationCurve BrightnessLoop;

    private float timer;

    public void Update() {
        float pct = GameManager.Instance.GetGamePercentage;
        timer += Time.deltaTime * Mathf.Lerp(StartSpeed, EndSpeed, pct);
        float brightness = BrightnessLoop.Evaluate(timer);
        Color baseColor = Color.Lerp(StartColor, EndColor, pct);
        Color crispColor = baseColor.ReplacePortion(Color.black, 1f - brightness);
        Color blurredColor = crispColor.ReplacePortion(Color.black, BlurredBlackPortion);
        SpriteCrisp.color = crispColor;
        SpriteBlurred.color = blurredColor;
    }

}
