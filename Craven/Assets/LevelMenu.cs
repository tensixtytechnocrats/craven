using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LevelMenu : MonoBehaviour
{
    public GameObject scenebutton;
    // Start is called before the first frame update
    void Start()
    {
        for (int i =1; i < SceneManager.sceneCountInBuildSettings; i++) {
            GameObject abutton = Instantiate(scenebutton, transform);
            abutton.GetComponent<SceneSelect>().sceneindex = i;
            TextMeshProUGUI title = abutton.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
            //Yep, I can only get the path of scenes that aren't loaded. Can't get the names
            //Really hacky "chop of .unity and assets/scenes/ ahead
            string scenepath = SceneUtility.GetScenePathByBuildIndex(i);
            title.text = scenepath.Substring(14);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
