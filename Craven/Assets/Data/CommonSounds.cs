﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "CommonSounds", menuName = "Data/CommonSounds")]
public class CommonSounds : ScriptableObject {

	public List<SoundCall> Sounds = new List<SoundCall>();

	public SoundCall Get(string name) { return Sounds.Where(e => e.soundUsed.name == name).First(); }

}
