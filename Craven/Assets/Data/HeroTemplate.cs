﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hero Template", menuName = "Data/Hero Template")]
public class HeroTemplate : ScriptableObject {

    public Sprite CharacterSprite;
    public Color TextColor;
    public Color LightColor;
    public Color GUIColor;

}
