﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HeroNames", menuName = "Data/HeroNames")]
public class HeroNames : ScriptableObject {

	public List<string> FirstNames = new List<string>();
	public List<string> LastNames = new List<string>();

}
