﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScoreData", menuName = "Data/ScoreData")]
public class ScoreData : ScriptableObject {

	public float pointsToWin = 100f;
	
	public float pointsPerKill = 1;
	public float pointsPerTorchSecond = 0.1f;

	public float percentStolenOnKill = 0.1f;

}
