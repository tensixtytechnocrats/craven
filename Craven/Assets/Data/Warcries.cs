﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Warcries", menuName = "Data/Warcries")]
public class Warcries : ScriptableObject {

	public List<string> Cries = new List<string>();

}
