﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : Environmental 
{
    private GameManager GM;

    public SFLight lightsource;
    public SFLight shadowsource;

    public AnimationCurve lightningIntensity;
    public AnimationCurve shadowIntensity;

    private float timer;

    // Start is called before theirst frame update
    public override void Start() {
        GM = GameManager.Instance;

        GM.addWindow(this);

        if (lightsource == null)
            lightsource = transform.Find("Lightning").GetComponent<SFLight>();
        if (shadowsource == null)
            shadowsource = transform.Find("WindowPattern").GetComponent<SFLight>();



        refresh();
    }

    public override void Update() {
        if(timer >= 0)
        {
            lightsource.intensity = lightningIntensity.Evaluate(timer);
            shadowsource.intensity = -shadowIntensity.Evaluate(timer);
            timer += Time.deltaTime;

            //Animation has hit end, reset everything
            if (lightningIntensity.Evaluate(timer) < 0 || shadowIntensity.Evaluate(timer) < 0)
                refresh();
        }
    }

    /// <summary>
    /// Causes a lightning strike, this will need to be called for all similarly rotated windows
    /// </summary>
    /// <returns>true means successfully struck</returns>
    public override void Animate() {
        timer = 0;
        return;
    }

    public override bool MakeNoise()
    {
        NoiseManager.Instance.MakeNoiseAtPoint(NoiseManager.NoiseSize.large, "CRAAAACK", (Vector2)transform.position);
        return base.MakeNoise();
    }

    /// <summary>
    /// Determines if lightning animation has started.
    /// </summary>
    /// <returns>Returns true if animating</returns>
    public bool isAnimating()
    {
        return (timer >= 0f);
    }


    /// <summary>
    /// Resets intensity of SF Lights
    /// </summary>
    private void refresh()
    {
        lightsource.intensity = 0;
        shadowsource.intensity = 0;
        timer = -1;
    }
}
